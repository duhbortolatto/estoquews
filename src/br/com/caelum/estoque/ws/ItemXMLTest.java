package br.com.caelum.estoque.ws;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.jupiter.api.Test;

import br.com.caelum.estoque.modelo.item.Item;

class ItemXMLTest {

	@Test
	void test() {
		Item item = new Item.Builder().comCodigo("MEA").comNome("MEAN").comQuantidade(4).comTipo("Livro").build();
		Marshaller marschaller;
		try {
			JAXBContext context = JAXBContext.newInstance(Item.class);
			marschaller = context.createMarshaller();
			marschaller.marshal(item, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

}
